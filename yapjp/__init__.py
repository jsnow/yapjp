from pathlib import Path
from typing import Any, BinaryIO, Iterator, Union

from .parser import Parser, ParserError
from .lexer import Lexer, LexerError


def parse_file(f: BinaryIO, chunk_size: int = 4096) -> Iterator[object]:
    parser = Parser()

    for document in parser:
        if document == ...:
            parser.feed(f.read(chunk_size))
            continue
        yield document


def parse_filename(filename: Union[str, Path]) -> Iterator[object]:
    parser = Parser()

    p = Path(filename)
    with open(p, "rb") as file:
        yield from parse_file(file)


def parse_buffer(data: Union[bytes, bytearray]) -> Iterator[object]:
    parser = Parser()
    parser.feed(data)
    parser.feed_eof()
    yield from parser


def parse(f: Any) -> Iterator[object]:
    """
    Try to parse JSON from whatever @f is.
    """
    # Raw data?
    if isinstance(f, (bytes, bytearray)):
        yield from parse_buffer(f)
        return

    # Filename?
    try:
        p = Path(f)
    except TypeError:
        pass  # nope...
    else:
        if p.exists():
            yield from parse_filename(p)
            return

    # File-like?
    if hasattr(f, "read"):
        yield from parse_file(f)
        return

    raise TypeError(
        "Expected bytes, bytearray, file-like object or path-like object, "
        f"not {type(f).__name__}"
    )


__all__ = [
    "Parser",
    "Lexer",
    "ParserError",
    "LexerError",
    "parse",
    "parse_file",
    "parse_filename",
    "parse_buffer",
]
