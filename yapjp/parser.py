from typing import (
    Callable,
    cast,
    Dict,
    Generator,
    Iterator,
    List,
    Optional,
    Self,
    Union,
    Sequence,
    TypeVar,
)
from types import EllipsisType, GeneratorType

from .lexer import Lexer, Token, TokenKind


class ParserError(RuntimeError):
    def __init__(self, msg: str, start: int, end: Optional[int] = None):
        # If end is omitted, span of error is assumed to be one byte.
        if end is None:
            end = start

        if end == start:
            suffix = f" (@context: 0x{start:02x})"
        else:
            suffix = f" (@context: [0x{start:02x} - 0x{end:02x}])"

        super().__init__(f"{msg}{suffix}")
        self.msg = msg
        self.start = start
        self.end = end


U = TypeVar("U")
V = TypeVar("V")


class Metadata:
    _comments: Sequence[str] = ()
    _tokens: Sequence[Token] = ()

    def comments(self) -> str:
        return "".join(self._comments)

    def raw(self) -> bytes:
        return b"".join(tok.raw for tok in self._tokens)


class MetaDict(dict[U, V], Metadata):
    pass


class MetaList(list[U], Metadata):
    pass


class MetaStr(str, Metadata):
    pass


class MetaInt(int, Metadata):
    pass


class MetaFloat(float, Metadata):
    pass


class MetaBool(int, Metadata):
    def __repr__(self) -> str:
        return "True" if self else "False"

    def __int__(self) -> int:
        return 1 if self else 0


class Parser:
    def __init__(self) -> None:
        self._lexer = Lexer()

        # Only ever used to put a single token back;
        # won't grow beyond 1 element as currently written.
        self._token_buffer: List[Token] = []

        # last token received via _next_token
        self._last_token: Token

        # Parser state
        self._iterobj: Optional[Iterator[Union[EllipsisType, object]]] = None

        self._parse_table = {
            TokenKind.T_LITERAL: ("literal", self._parse_literal),
            TokenKind.T_STRING: ("string", self._parse_string),
            TokenKind.T_NUMBER: ("number", self._parse_number),
            TokenKind.T_LBRACKET: ("array", self._parse_array),
            TokenKind.T_LBRACE: ("object", self._parse_object),
        }

        self._object_hooks: Dict[str, Callable[..., object]] = {
            "bool": MetaBool,
            "int": MetaInt,
            "float": MetaFloat,
            "array": MetaList,
            "object": MetaDict,
            "string": MetaStr,
            "null": lambda: None,
        }

    def __iadd__(self, other: object) -> Self:
        if (other is not None) and (not isinstance(other, (bytes, bytearray))):
            return NotImplemented
        self.feed(other)
        return self

    def feed(self, data: Union[None, bytes, bytearray]) -> None:
        self._lexer.feed(data)

    def feed_eof(self) -> None:
        self._lexer.feed_eof()

    @property
    def last_position(self) -> int:
        return self._last_token.pos.start

    def __iter__(self) -> Iterator[Union[EllipsisType, object]]:
        # Because the parser may yield in the middle of trying to
        # parse a compound value, it's important we only ever have one
        # iterator so that parsing always resumes where it left off.
        if not self._iterobj:
            self._iterobj = self._parse()
        return self._iterobj

    def __next__(self) -> Union[EllipsisType, object]:
        # It's safe to do because of the _iter/__iter__ paradigm above.
        return next(iter(self))

    def _next_token(
        self, tokens: List[Token], skip_ws: bool = True, skip_comments: bool = True
    ) -> Generator[EllipsisType, None, Token]:
        """
        This generator is used to get the next *token* in the stream.

        Usage: `token = (yield from self._next())`

        :yields: A sequence of zero or more Ellipsis objects
        :returns: The next Token in the lexer stream.
        """
        while True:
            if self._token_buffer:
                tokens.append(self._token_buffer[0])
                return self._token_buffer.pop(0)
            try:
                token = next(self._lexer)
            except StopIteration:
                raise EOFError

            # because we may eventually yield legitimate None values,
            # our "not enough data" value needs to be something else:
            # I chose the ellipsis...................................
            if token is None:
                yield ...
            else:
                tokens.append(token)
                if token.kind == TokenKind.T_WHITESPACE and skip_ws:
                    continue
                if token.kind == TokenKind.T_COMMENT and skip_comments:
                    continue
                self._last_token = token
                return token

    def _parse_array(
        self, tokens: List[Token]
    ) -> Generator[EllipsisType, None, List[object]]:
        array_start = self.last_position
        ret = cast(List[object], self._object_hooks["array"]())

        token = yield from self._next_token(tokens)
        if token.kind == TokenKind.T_RBRACKET:
            return ret
        tokens.pop(-1)
        self._token_buffer.append(token)  # (un-get this token!)

        while True:
            # Get an element for the array:
            value = yield from self._parse_value(tokens)
            ret.append(value)

            # What next?
            token = yield from self._next_token(tokens)
            if token.kind == TokenKind.T_RBRACKET:
                break
            if token.kind != TokenKind.T_COMMA:
                raise ParserError(
                    f"unexpected token {token} in array; expected RBRACKET or COMMA",
                    array_start,
                    token.pos.start,
                )

        return ret

    def _parse_object(
        self, tokens: List[Token]
    ) -> Generator[EllipsisType, None, Dict[str, object]]:
        object_start = self.last_position
        ret = cast(Dict[str, object], self._object_hooks["object"]())

        def _error(token: Token, expected: str) -> None:
            raise ParserError(
                f"unexpected token {token} in object; expected {expected}",
                object_start,
                token.pos.start,
            )

        token = yield from self._next_token(tokens)
        if token.kind == TokenKind.T_RBRACE:
            return ret
        if token.kind != TokenKind.T_STRING:
            _error(token, "RBRACE or STRING")

        while True:
            if token.kind != TokenKind.T_STRING:
                _error(token, "STRING")
            key = token.value

            token = yield from self._next_token(tokens)
            if token.kind != TokenKind.T_COLON:
                _error(token, "COLON")

            value = yield from self._parse_value(tokens)
            ret[key] = value

            token = yield from self._next_token(tokens)
            if token.kind == TokenKind.T_RBRACE:
                break
            elif token.kind != TokenKind.T_COMMA:
                _error(token, "COMMA or RBRACE")

            token = yield from self._next_token(tokens)

        return ret

    def _parse_literal(self, tokens: List[Token]) -> Union[None, bool, float]:
        value: Union[None, bool, float]
        token = self._last_token
        assert token.value in ("true", "false", "null", "NaN")
        if token.value == "true":
            value = cast(bool, self._object_hooks["bool"](True))
        elif token.value == "false":
            value = cast(bool, self._object_hooks["bool"](False))
        elif token.value == "null":
            value = cast(None, self._object_hooks["null"]())
        elif token.value == "NaN":
            value = cast(float, self._object_hooks["float"]("NaN"))
        return value

    def _parse_string(self, tokens: List[Token]) -> str:
        token = self._last_token
        return cast(str, self._object_hooks["string"](token.value))

    def _parse_number(self, tokens: List[Token]) -> Union[float, int]:
        token = self._last_token
        value: Union[float, int]
        if token.value in ("Infinity", "-Infinity", "NaN"):
            value = cast(float, self._object_hooks["float"](token.value))
        elif "e" in token.value or "E" in token.value or "." in token.value:
            value = cast(float, self._object_hooks["float"](token.value))
        else:
            value = cast(int, self._object_hooks["int"](token.value))
        return value

    def _parse_value(
        self, tokens: Optional[List[Token]] = None
    ) -> Generator[EllipsisType, None, object]:
        comments: List[str] = []
        local_tokens: List[Token] = []

        while True:
            token = yield from self._next_token(local_tokens, skip_comments=False)
            value: object

            if token.kind == TokenKind.T_COMMENT:
                comments.append(token.value)
                continue

            if token.kind in self._parse_table:
                what, method = self._parse_table[token.kind]
                value = method(local_tokens)
                if isinstance(value, GeneratorType):
                    try:
                        value = yield from value
                    except EOFError:
                        raise ParserError(
                            f"unexpected EOF while parsing {what}",
                            token.pos.start,
                            self.last_position,
                        )
            else:
                raise ParserError(
                    f"expected JSON value; got {token}",
                    token.pos.start,
                    self.last_position,
                )

            break

        if comments and hasattr(value, "_comments"):
            # true/false/null don't support metadata :(
            value._comments = comments
        if hasattr(value, "_tokens"):
            if isinstance(value._tokens, list):
                value._tokens.extend(local_tokens)
            else:
                value._tokens = local_tokens
        if tokens is not None:
            tokens.extend(local_tokens)

        return value

    def _parse(self) -> Iterator[Union[EllipsisType, object]]:
        while True:
            try:
                value = yield from self._parse_value(None)
                yield value
            except EOFError:
                return
