"""
ftfy_util.py - Borrowed utilities from FTFY

The code in this module was copied and/or modified from Robyn Speer's FTFY,
which is licensed via Apache 2.0.

https://github.com/rspeer/python-ftfy
"""
import re


SURROGATE_PAIR_RE = re.compile("[\ud800-\udbff][\udc00-\udfff]")


# This function copied verbatim from FTFY.
def convert_surrogate_pair(match: re.Match[str]) -> str:
    """
    Convert a surrogate pair to the single codepoint it represents.

    This implements the formula described at:
    http://en.wikipedia.org/wiki/Universal_Character_Set_characters#Surrogates
    """
    pair = match.group(0)
    codept = 0x10000 + (ord(pair[0]) - 0xD800) * 0x400 + (ord(pair[1]) - 0xDC00)
    return chr(codept)


# This function and docstring modified to allow orphaned surrogates,
# which matches Python's JSON parser's behavior.
def fix_surrogates(text: str) -> str:
    """
    Replace 16-bit surrogate codepoints with the characters they represent
    (when properly paired).

        >>> high_surrogate = chr(0xd83d)
        >>> low_surrogate = chr(0xdca9)
        >>> print(fix_surrogates(high_surrogate + low_surrogate))
        💩
    """
    if SURROGATE_PAIR_RE.search(text):
        text = SURROGATE_PAIR_RE.sub(convert_surrogate_pair, text)
    return text
