"""
lexer.py - lex streaming JSON input

This module provides a streaming JSON lexer. It lexes a stream of bytes
into a stream of Tokens, which may then be parsed. This lexer *generally*
does not interpret the tokens, choosing to represent their values as
strings. String tokens are an "exception" in that escape sequences are
processed up-front, and enclosing quotes elided.

The design  of this lexer  is unusual in  that instead of  reading data,
data is fed  into it via the `Lexer.feed()` method.  The lexer considers
the stream to be unterminated until `Lexer.feed_eof()` is called (or EOF
is fed to `feed()`). This design enables the lexer to be used both for
synchronous cases where the entire document can be buffered, and for
asynchronous cases where the document may not yet be complete.

The lexer can be used as either an Iterable or an Iterator (or both);
i.e. all of the following forms are valid and supported:

As an Iterable::

    lexer = Lexer()
    for token in lexer:
        ...

As an Iterator::

    lexer = Lexer()
    token = next(lexer)

As both::

    lexer = Lexer()
    it = iter(lexer)
    token_a = next(it)
    token_b = next(lexer)

The logic for allowing this mixed/flexible usage is defined between
`_lex`, `__iter__`, and `__next__`.

The root of the lexing logic starts at `Lexer._lex()`, which is a
generator method that may yield a `None` value whenever there is
insufficient data to yield the next `Token`. This returns control to the
caller, which can then buffer more data and feed it to the lexer.

Example::
    lexer = Lexer()
    file = open("some_json_file", "rb")

    for token in lexer:
        if token is None:
            lexer.feed(file.read(4096))
            continue
        print(token)
    print("Lexing is finished!")

"""

from dataclasses import dataclass
import re
from enum import Enum
from typing import (
    Callable,
    Dict,
    Generator,
    Iterator,
    List,
    Optional,
    Self,
    Set,
    Tuple,
    Union,
)

from .ftfy_util import fix_surrogates


WS_DATA_RE = re.compile(rb"[\r\n\t ]+")


# escaped byte to replacement value table.
# This is all possible single-byte replacements, not necessarily which
# are configured/allowed for a given instance.
ESCAPE_TABLE = {
    0x22: b'"',  # \" - double quote
    0x27: b"'",  # \' - single quote
    0x2F: b"/",  # \/ - "solidus". (Who named it that?)
    0x30: b"\0",  # \0 - null
    0x5C: b"\x5C",  # backslash ("reverse solidus")
    0x62: b"\b",  # \b - backspace
    0x66: b"\f",  # \f - formfeed
    0x6E: b"\n",  # \n - newline
    0x72: b"\r",  # \r - carriage return
    0x74: b"\t",  # \t - horizontal tab
    0x76: b"\v",  # \v - vertical tab
}

ST_ESCAPE_TABLE = {
    '"': '"',
    "'": "'",
    "/": "/",
    "\\": "\\",
    "b": "\b",
    "f": "\f",
    "n": "\n",
    "r": "\r",
    "t": "\t",
}


def json_unescape(match: re.Match[bytes]) -> bytes:
    """
    Process escape sequences in a JSON string (represented as bytes).
    """
    code = match.group(0)[1]
    if code == 0x75:
        char = chr(int(match.group(0)[2:], base=16))
        return char.encode("UTF-8", errors="surrogatepass")
    return ESCAPE_TABLE[match.group(0)[1]]


def json_unescape_str(match: re.Match[str]) -> str:
    """
    Process escape sequences in a JSON string.

    (TODO: Decide on if it's faster to work in bytes or str, and
    eliminate one or the other helper. Optimization work is needed
    here.)
    """
    code = match.group(0)[1]
    if code == "u":
        return chr(int(match.group(0)[2:], base=16))
    return ST_ESCAPE_TABLE[code]


def fmt_size(n: float) -> str:
    """Format bytes @n as a human-readable size"""
    suffices = ["B", "KiB", "MiB", "GiB"]
    lim = n
    for suffix in suffices:
        if lim < 512 or suffix == "GiB":
            break
        lim = lim / 1024.0
    return f"{lim} {suffix}"


class Position:
    """Represents the position of a token in a byte stream."""

    __slots__ = "start", "end"

    def __init__(self, start: int, end: int):
        self.start = start
        self.end = end

    def __str__(self) -> str:
        if self.end == self.start + 1:
            # No need to print a range for one byte.
            return f"[0x{self.start:02x}]"
        return f"[0x{self.start:02x} - 0x{self.end - 1:02x}]"

    def __eq__(self, other: object) -> bool:
        if not isinstance(other, Position):
            return NotImplemented
        return (self.start, self.end) == (other.start, other.end)

    def __add__(self, other: object) -> Self:
        if not isinstance(other, int):
            return NotImplemented
        return type(self)(self.start + other, self.end + other)

    def __sub__(self, other: object) -> Self:
        if not isinstance(other, int):
            return NotImplemented
        return type(self)(self.start - other, self.end - other)

    def copy(self) -> Self:
        """Return a mutable copy of this object."""
        return type(self)(self.start, self.end)


class LexerError(RuntimeError):
    """
    Error class that represents a Lexer error occurring with a specific Token.

    :param token: The partial token where the failure was encountered.
                  The token should have at least one error context set on it.
    :param hint: A small contextual hint appended to the broadest error context.
                 This hint will be formatted with the malformed data.
                 e.g. ", partial token was {0}" may produce the error message:
                 "Unexpected EOF in number, partial token was '-10.'"
    """

    def __init__(self, token: "Token", hint: str):
        hint = hint.format(
            # Not acceptable to choke on an error print.
            # If there's UTF shenanigans, just escape them.
            repr(token.raw.decode("UTF-8", errors="backslashreplace"))
        )
        assert token._context
        msg = f"{token._context[-1][0]}{hint} @ {str(token.pos)}"
        for ctx in reversed(token._context[:-1]):
            msg += f"\n\t{ctx[0]} @ {ctx[1]}"

        super().__init__(msg)
        self.msg = msg
        self.pos = token.pos


class TokenKind(Enum):
    """
    Enumeration representing possible Token values in the stream.
    """

    # Single-character structural tokens
    T_LBRACE = "{"
    T_RBRACE = "}"
    T_COLON = ":"
    T_COMMA = ","
    T_LBRACKET = "["
    T_RBRACKET = "]"

    # Variable length tokens
    T_WHITESPACE = "(ws)"
    T_STRING = "(string)"
    T_LITERAL = "(literal)"
    T_NUMBER = "(number)"
    T_JUNK = "(junk)"
    T_COMMENT = "(comment)"


STRUCTURAL_LUT = {
    0x3A: TokenKind.T_COLON,
    0x2C: TokenKind.T_COMMA,
    0x7B: TokenKind.T_LBRACE,
    0x7D: TokenKind.T_RBRACE,
    0x5B: TokenKind.T_LBRACKET,
    0x5D: TokenKind.T_RBRACKET,
}


class Token:
    """
    A lexer Token.

    After lexing, the raw property will contain the raw data comprising
    the token, while value will contain the processed token as a string.
    For Tokens representing errors, the print_detail method can be used
    to get a detailed error report.

    :param kind: The TokenKind for this Token.
    :param start: The byte index that starts this token.
    :param start_line: The line on which this token starts.
    :param end: The byte index that ends this token.
                Can be omitted, but should be filled in later.
    :param end_line: The line on which this token ends.
                     Can be omitted, but should be filled in later.
    """

    __slots__ = ("kind", "pos", "start_line", "end_line", "raw", "_value", "_context")

    def __init__(
        self,
        kind: TokenKind,
        start: int,
        start_line: int,
        end: int = -1,
        end_line: int = -1,
    ):
        self.kind = kind
        self.pos = Position(start, end)  # Absolute stream position.
        self.start_line = start_line
        self.end_line = end_line
        self.raw = bytearray()

        self._value: Optional[str] = None
        # Contextual ranges for highlighting failures.
        self._context: Optional[List[Tuple[str, Position]]] = None

    def __repr__(self) -> str:
        fields = [
            self.kind.name,
            repr(self.value),
        ]
        fields.append(f"@ {str(self.pos)}")

        if self._context:
            context = "; ".join((c[0] for c in reversed(self._context)))
            fields.append(f"({context})")

        return f"<{' '.join(fields)}>"

    @property
    def name(self) -> str:
        """
        The name of this Token's type.

        Matches TokenKind enumeration names with the leading "T_" removed.
        """
        return self.kind.name[2:]

    @property
    def value(self) -> str:
        """The string value of this Token."""
        # { } [ ] , :
        if len(self.kind.value) == 1:
            return self.kind.value

        # Everything else: whitespace, strings, literals
        # (true/false/null), numbers
        assert self._value is not None
        return self._value

    def add_context(self, msg: str, pos: Position) -> None:
        """
        Add error context to this token.

        The most detailed error information should be added first,
        followed by the most general errors.
        """
        if self._context is None:
            self._context = []
        self._context.append((msg, pos))

    def finalize(self, encoding: str) -> None:
        """
        Finalize the value of this token.

        String values have escapes processed and enclosing quotes removed.
        Comments have their leading comment glyph and trailing newline removed.
        Everything else is encoded as a string as-is.

        Comments and Junk tokens are decoded using backslashreplace to
        handle encoding problems so that they *cannot* fail to
        process. Strings permit orphaned surrogates using
        "surrogatepass", but *may* fail on other decoding errors.
        """
        if self.kind == TokenKind.T_STRING:
            strdata = self.raw.decode(encoding, errors="surrogatepass")[1:-1]
            strdata = re.sub(
                r"\\(?:[\"\\/bfnrt]|u[0-9A-Fa-f]{4})",
                json_unescape_str,
                strdata,
            )
            self._value = fix_surrogates(strdata)

            # # Unescape values and remove enclosing quotes
            # tmp = re.sub(
            #     rb'\x5c([\x22\x5c/bfnrt]|u[0-9A-Fa-f]{4})',
            #     json_unescape,
            #     memoryview(self.raw)[1:-1],
            # )

            # # strip quotes and interpret surrogates
            # self._value = fix_surrogates(
            #     tmp.decode(encoding, errors="surrogatepass")
            # )
        elif self.kind == TokenKind.T_COMMENT:
            # Remove leading '#' or '# ' and trailing newline
            match = re.match(b"# ?(.*)\r?\n$", self.raw)
            assert match
            self._value = match.group(1).decode(encoding, errors="backslashreplace")
        elif self.kind == TokenKind.T_JUNK:
            self._value = self.raw.decode(encoding, errors="backslashreplace")
        elif len(self.kind.value) != 1:
            # As long as it isn't a structural token
            self._value = self.raw.decode(encoding)

    def print_detail(self) -> None:
        """
        Print detailed context information for a token.

        If the token is a junk token, print detailed error context.
        """
        print(f"{self.name.capitalize()}: {self.value!r} @ {self.pos}")
        if self.kind != TokenKind.T_JUNK or not self._context:
            return

        if self.pos != self._context[-1][1]:
            print(f" -> @{self.pos} Rejected token")

        for ctx in reversed(self._context):
            print(f" -> @{ctx[1]} {ctx[0]}")

            # Rebase position to the start of this token
            idx = ctx[1] - self.pos.start

            # Is this a wasteful way to compute this? Sure, maybe.
            # It sure does save a lot of heartache trying to count columns, tho.
            n_skip = len(repr(bytes(self.raw[: idx.start]))[2:-1]) + 2
            n_hilite = len(repr(bytes(self.raw[idx.start : idx.end]))[2:-1])

            print(f"     => {bytes(self.raw)!r}")
            print(f"        {' ' * n_skip}{'^' * n_hilite}")


class Lexer:
    @dataclass
    class Options:
        # Maximum allowed size of the pending buffer
        buffer_limit: int = 10485760

        # Emit junk tokens, or raise LexerError on unexpected bytes?
        emit_junk: bool = True

        # Allow +Infinity, -Infinity and NaN literals
        allow_nan: bool = True

        # Allow singly quoted strings and the \' escape sequence.
        # Strings must still use matching quotes.
        allow_single_quote: bool = False

        # Allow Python-style line comments
        allow_comments: bool = False

    # O:-)

    def __init__(self) -> None:
        # Lexer options: Can be changed before lexing begins.
        self.opt = Lexer.Options()

        # Byte index of the first character currently "under
        # consideration"; i.e. the absolute index of _buffer[0].
        self._pos = 0

        # Current line location
        self._line = 1

        # Incoming data buffered from `feed()`.
        self._buffer = bytearray()

        # Simply marks if feed_eof() was called.
        self._eof = False

        # Partial token that is currently being formed.
        # Used for literals, strings, whitespace, and numbers.
        # (Not used for structural characters.)
        self._pend = Token(TokenKind.T_JUNK, -1, -1)

        # https://datatracker.ietf.org/doc/html/rfc8259#section-8.1
        # "JSON text exchanged between systems that are not part of a
        # closed ecosystem MUST be encoded using UTF-8 [RFC3629]."
        #
        # Change at your own risk: any encoding you decide to squeeze in
        # here must support errors='surrogatepass' for encoding and
        # decoding operations.
        self._encoding = "UTF-8"

        # Generator object for the lexer.
        self._iterobj: Optional[Iterator[Optional[Token]]] = None

        # Dispatch tables: filled in by _compile() on iterator entrance.
        self._table: Dict[int, Callable[[], Iterator[Optional[Token]]]] = {}
        self._allowed_escapes: Set[int] = set()

    def _compile(self) -> None:
        """
        Compile the lookup/dispatch tables.

        This is an optimization that helps remove conditionals in the
        hotpath owing to Lexer options, in exchange for a one-time
        startup fee.

        Note that structural tokens are not part of this LUT;
        they're hardcoded directly in _lex() instead.
        """
        for i in range(256):
            self._table[i] = self._lex_junk
        self._allowed_escapes = {0x22, 0x2F, 0x5C, 0x62, 0x66, 0x6E, 0x72, 0x74}

        self._table[ord("t")] = lambda: self._lex_literal(b"true")
        self._table[ord("f")] = lambda: self._lex_literal(b"false")
        self._table[ord("n")] = lambda: self._lex_literal(b"null")
        for char in b" \r\n\t":
            self._table[char] = self._lex_whitespace
        for char in b"-0123456789":
            self._table[char] = self._lex_number
        self._table[ord('"')] = self._lex_string

        if self.opt.allow_nan:
            self._table[ord("I")] = self._lex_number
            self._table[ord("N")] = lambda: self._lex_literal(b"NaN")

        if not self.opt.allow_single_quote:
            # Any bytes except whitespace, structural token, or double quote.
            self._JUNK_RE = re.compile(rb'[^\r\n\t {}:,\[\]"]+')
        else:
            self._table[ord("'")] = self._lex_string
            self._allowed_escapes.add(0x27)
            # Same as above, but additionally exclude the single quote.
            self._JUNK_RE = re.compile(rb'[^\r\n\t {}:,\[\]"\']+')

        if self.opt.allow_comments:
            self._table[ord("#")] = self._lex_comment

        self._JUNK_STRING_SINGLE_RE = re.compile(rb"[^']+")
        self._JUNK_STRING_DOUBLE_RE = re.compile(rb'[^"]+')

        def _string_pattern(quote: bytes) -> re.Pattern[bytes]:
            # The trailing '+' here is technically superfluous,
            # but *goddess* is it slow without it.
            data = rb"[^\x00-\x1F\\" + quote + rb"]+"
            escape = rb"\\[" + re.escape(bytes(self._allowed_escapes)) + rb"]"
            uescape = rb"\\u[0-9A-Fa-f]{4}"
            patt = b"".join((b"(?:", data, b"|", escape, b"|", uescape, b")+"))
            patt = patt + b"(" + quote + b")?"
            return re.compile(patt)

        self._STRING_DOUBLE_RE = _string_pattern(b"\x22")
        self._STRING_SINGLE_RE = _string_pattern(b"\x27")

        number_core = (
            b"(?:0|[1-9][0-9]*)"  # base, mandatory
            rb"(?:\.[0-9]+)?"  # fraction, optional
            b"(?:[eE][-+]?[0-9]+)?"  # exponent, optional
        )

        if self.opt.allow_nan:
            # NaN is handled directly by the literal lexer,
            # because it doesn't take a sign.
            self._NUMBER_RE = re.compile(
                b"".join(
                    (
                        b"-?",  # sign, optional
                        b"(?:",  # group: numerical or literal
                        number_core,  # numerical
                        b"|",  # -or-
                        b"Infinity",  # inf literal
                        b")",  # end group: numerical or constant
                    )
                )
            )
        else:
            self._NUMBER_RE = re.compile(
                b"".join(
                    (
                        b"-?",  # sign, optional
                        number_core,  # numerical
                    )
                )
            )

    def __iadd__(self, other: object) -> Self:
        # Syntactic sugar for feed() to allow lexer += bytes syntax.
        if not isinstance(other, (bytes, bytearray)):
            return NotImplemented
        self.feed(other)
        return self

    def feed(self, data: Union[None, bytes, bytearray]) -> None:
        """
        Feed data into the lexer.

        Reciprocates the return values from `io.RawIOBase.read()`:
         * "None" is allowed, but ignored and has no effect.
           (We assume more data will arrive eventually.)
         * b'' is treated as EOF, and `feed_eof()` is called.
        """
        if data is None:
            # data source is non-blocking and has no data available
            return

        if data == b"":
            self.feed_eof()
            return

        if self._eof:
            raise RuntimeError("Cannot feed bytes into the lexer after EOF!")

        buf_size = len(data) + len(self._buffer)
        if buf_size > self.opt.buffer_limit:
            buf_size_str = fmt_size(buf_size)
            limit = fmt_size(self.opt.buffer_limit)
            raise RuntimeError(
                f"Buffer would grow to {buf_size_str}; "
                f"Cannot buffer more than {limit}"
            )
        self._buffer.extend(data)

    def feed_eof(self) -> None:
        """
        Explicitly signal EOF.

        This has the same effect as calling `feed(b'')`.
        """
        self._eof = True

    def peek(self) -> Generator[None, None, int]:
        """
        Peek at the first byte in the lexing buffer.

        This function is a generator that yields a sequence of None
        objects as necessary until there is at least a single byte in
        the buffer. The generator will then *return* that byte.

        Does not modify the buffer nor advance the cursor.

        Using this method requires that the calling function also be a
        generator that can optionally yield None.

        Usage::

            char = (yield from self.peek())
        """
        while not self._buffer:
            if self._eof:
                raise EOFError
            yield None
        return self._buffer[0]

    def _expect(self, kind: TokenKind) -> None:
        """Initialize a pending token and accompanying buffer."""
        self._pend = Token(kind, self._pos, self._line)

    def _advance(self, n: int = 1) -> None:
        """Discard @n bytes from the incoming buffer."""
        self._pos += n
        del self._buffer[:n]

    def _accept(self, n: int = 1) -> None:
        """Move @n bytes from the incoming buffer into the pending Token."""
        self._pend.raw.extend(self._buffer[:n])
        self._pos += n
        del self._buffer[:n]

    def _accept_token(self) -> Token:
        """Finalize a pending Token and return it."""
        token = self._pend
        token.pos.end = self._pos
        token.end_line = self._line

        try:
            token.finalize(self._encoding)
        except Exception as exc:
            # Unicode problems, usually.
            emsg = f"Failed to finalize {token.kind.name} data"

            # Recontextualize error position, if possible:
            if isinstance(exc, UnicodeDecodeError):
                pos = Position(exc.start, exc.end) + token.pos.start
                token.add_context(str(exc), pos)
            else:
                token.add_context(str(exc), token.pos)

            token.add_context(emsg, token.pos)
            if not self.opt.emit_junk:
                raise LexerError(token, ", data was {0}") from exc

            # May you go with god, brave token
            assert token.kind != TokenKind.T_JUNK
            token.kind = TokenKind.T_JUNK
            token.finalize(self._encoding)

        del self._pend
        return token

    def _reject_token(
        self,
        emsg: str,
        hint: str = ", partial token was {0}",
        context: Optional[Tuple[str, int]] = None,
        eof: bool = False,
    ) -> Iterator[Optional[Token]]:
        """Reject a pending token."""
        if not eof:
            # "accept" the rejected byte into this now-failed token:
            self._accept()

        token = self._pend
        token.pos.end = self._pos
        token.end_line = self._line

        if context:
            # Rejected subregion: additional context, when applicable.
            self._pend.add_context(context[0], Position(context[1], token.pos.end))
        # Rejected token range: the full span of the initial rejected token,
        # before we possibly extend it with junk.
        self._pend.add_context(emsg, token.pos.copy())

        # If we are emitting JUNK tokens instead of raising exceptions,
        # continue to lex for additional junk.
        if self.opt.emit_junk:
            yield from self._lex_junk(keep_token=True)
            del self._pend
            return

        # Otherwise, format this as an Exception and raise it.
        try:
            raise LexerError(token, hint)
        finally:
            del self._pend

    def __iter__(self) -> Iterator[Optional[Token]]:
        if not self._iterobj:
            # Kick the Generator to grab the first yield,
            # Which allows the paranoia check.
            it = self._lex()
            assert next(it) is None
            self._iterobj = it
        return self._iterobj

    def __next__(self) -> Optional[Token]:
        return next(iter(self))

    def _lex_literal(
        self, literal: bytes, new_token: bool = True
    ) -> Iterator[Optional[Token]]:
        """
        Called when the lexer encounters a character that starts a literal.

        @literal should be b'true', b'false', or b'null'. If
        `allow_nan` is True, then it can also be b'NaN' or b'Infinity'.
        """
        if new_token:
            self._expect(TokenKind.T_LITERAL)
            ref_literal = literal
        else:
            # We may have existing data in the buffer, i.e. "-" for
            # -Infinity that we want to use for the error message.
            ref_literal = self._pend.raw + literal

        # Fast: If we have the entire literal buffered already
        t_len = len(literal)
        if self._buffer[:t_len] == literal:
            self._accept(t_len)
            yield self._accept_token()
            return

        # Slow: We need to wait for more data to decide
        for expected in literal:
            peek = yield from self.peek()
            if peek != expected:
                yield from self._reject_token(
                    f"Expected literal {ref_literal.decode()!r}",
                    hint=", but got {0}",
                )
                return
            self._accept()

        yield self._accept_token()

    def _lex_whitespace(self) -> Iterator[Optional[Token]]:
        """
        Called when the lexer encounters a whitespace character.

        Does not fully return until a non-whitespace character is found.
        """
        self._expect(TokenKind.T_WHITESPACE)

        while True:
            match = WS_DATA_RE.match(self._buffer)
            if match is None:
                break
            self._accept(match.end())

            if self._buffer:
                # Data left in buffer - WS token is finished.
                break

            try:
                yield from self.peek()
            except EOFError:
                break

        self._line += self._pend.raw.count(b"\n")
        yield self._accept_token()

    def _lex_escape(self) -> Generator[Optional[Token], None, bool]:
        escape_pos = self._pos
        self._accept()

        peek = yield from self.peek()
        if peek == 0x75:  # \u
            self._accept()
            for _ in range(4):
                peek = yield from self.peek()
                if peek not in b"0123456789abcdefABCDEF":
                    yield from self._reject_token(
                        "Invalid string",
                        context=("Invalid unicode escape", escape_pos),
                    )
                    return False
                self._accept()
        elif peek in self._allowed_escapes:
            self._accept()
        else:
            yield from self._reject_token(
                "Invalid string",
                context=("Unrecognized escape", escape_pos),
            )
            return False

        return True

    def _lex_string(self) -> Iterator[Optional[Token]]:
        """
        Called when the lexer encounters an opening quote.
        """
        self._expect(TokenKind.T_STRING)
        self._accept()  # eat the opening quote

        quote: int = self._pend.raw[-1]
        if quote == 0x22:  # "
            regex = self._STRING_DOUBLE_RE
        elif quote == 0x27:  # '
            regex = self._STRING_SINGLE_RE
        else:
            assert False, f"bug in string lexer - unhandled quote {chr(quote)}"

        while True:
            # Excludes control characters, incomplete/malformed escape
            # sequences, and the proper quote type.
            match = regex.match(self._buffer)
            if match:
                # If we check this *after* accept, it's a memory safety bug >:(
                has_close = bool(match.group(1))
                self._accept(match.end())
                if has_close:
                    break

            peek = yield from self.peek()
            if peek <= 0x1F:
                # https://datatracker.ietf.org/doc/html/rfc8259#section-7
                #
                # All Unicode characters may be placed within the
                # quotation marks, except for the characters that MUST
                # be escaped: quotation mark, reverse solidus, and the
                # control characters (U+0000 through U+001F).
                yield from self._reject_token(
                    "Invalid string",
                    context=("Illegal character", self._pos),
                )
                return

            if peek == quote:
                self._accept()
                break

            if peek == 0x5C:
                # Start of an incomplete or malformed escape sequence.
                if not (yield from self._lex_escape()):
                    return

            # Normal string data - fall through to regex again.

        yield self._accept_token()

    def _lex_comment(self) -> Iterator[Optional[Token]]:
        self._expect(TokenKind.T_COMMENT)
        self._accept()

        while True:
            try:
                peek = yield from self.peek()
            except EOFError:
                break
            self._accept()
            if peek == 0x0A:
                self._line += 1
                break

        yield self._accept_token()

    def _lex_digits(
        self, what: str, span: str = "[0-9]"
    ) -> Generator[Optional[Token], None, bool]:
        # Note: the span parameter is solely for the error message.
        #
        # In the one case where we set it to [1-9], it's impossible to
        # get a zero from the stream because the caller has already
        # peeked and checked for a zero.
        start_pos = self._pos

        while True:
            try:
                char = yield from self.peek()
            except EOFError:
                if self._pos != start_pos:
                    # Got at least one digit; fine.
                    break
                raise

            if 48 <= char <= 57:  # Any digit [0-9]
                self._accept()
            else:
                # Got at least one digit; just exit
                if self._pos != start_pos:
                    break
                yield from self._reject_token(
                    "Invalid number",
                    hint=", got {0}",
                    context=(f"Expected {what} digit {span}", start_pos),
                )
                return False

        return True

    def _lex_number(self) -> Iterator[Optional[Token]]:
        self._expect(TokenKind.T_NUMBER)

        # Fast path: we have the *entire* number already buffered
        # (This requires the buffer to be longer than the match!)
        match = self._NUMBER_RE.match(self._buffer)
        if match and match.end() > len(self._buffer):
            self._accept(match.end())
            yield self._accept_token()
            return

        # Slow path: verify each component individually.

        # Negative sign, optional
        if (yield from self.peek()) == 0x2D:  # "-"
            self._accept()

        if self.opt.allow_nan:
            # "Infinity" or "-Infinity"
            if (yield from self.peek()) == 0x49:  # "I"
                yield from self._lex_literal(b"Infinity", new_token=False)
                return

        # Integral base, mandatory
        if (yield from self.peek()) == 0x30:  # "0"
            self._accept()
        else:
            if not (yield from self._lex_digits("integral", "[1-9]")):
                return

        for _ in range(1):
            # Fractional component, optional
            try:
                peek = yield from self.peek()
            except EOFError:
                break  # Optional; not a problem.
            if peek == 0x2E:  # "."
                self._accept()
                if not (yield from self._lex_digits("fractional")):
                    return

            # Exponent, optional
            try:
                peek = yield from self.peek()
            except EOFError:
                break  # Optional; not a problem.
            if peek in b"eE":
                self._accept()
                # Technically optional, but if this is EOF, it's a problem, ...
                if (yield from self.peek()) in b"+-":
                    self._accept()
                # ... because this is not optional:
                if not (yield from self._lex_digits("exponential")):
                    return

        yield self._accept_token()

    def _lex_junk(self, keep_token: bool = False) -> Iterator[Optional[Token]]:
        """
        Called by the main dispatch to handle an unrecognized byte.

        When emit_junk is True, this method is also called by
        _reject_token to accrue any additional junk in the stream;
        keep_token will be True in this case.

        When emit_junk is False, this method stops immediately at the
        first unrecognized byte instead of lexing the "full" token.
        """
        regex = self._JUNK_RE
        terminus = None

        if keep_token:
            # We're gonna pad additional junk on existing token, now rejected
            assert self.opt.emit_junk
            if self._pend.kind == TokenKind.T_STRING:
                if self._pend.raw[0] == 0x22:
                    terminus = 0x22
                    regex = self._JUNK_STRING_DOUBLE_RE
                elif self._pend.raw[0] == 0x27:
                    terminus = 0x27
                    regex = self._JUNK_STRING_SINGLE_RE
            self._pend.kind = TokenKind.T_JUNK
        else:
            self._expect(TokenKind.T_JUNK)

        if not self.opt.emit_junk:
            assert not keep_token
            yield from self._reject_token("Unexpected byte", hint=" {0}")
            return

        while True:
            try:
                data = yield from self.peek()
            except EOFError:
                break

            # For rejected strings, grab the closing quote and bail.
            if data == terminus:
                self._accept()
                break

            # Usually: whitespace, quote, or structural char is enough to break out.
            # strings: we'll find the closing quote.
            match = regex.match(self._buffer)
            if match is None:
                break
            self._accept(match.end())

        self._line += self._pend.raw.count(b"\n")
        yield self._accept_token()

    def _lex(self) -> Iterator[Optional[Token]]:
        """
        Perform lexing and yield a sequence of Tokens.

        This generator must only be instantiated precisely once.

        Yields `None` when there isn't enough data in the lexing buffer to
        yield the next token. If no data is added to the lexing buffer,
        the stream of None objects will be infinite, so don't do
        anything like `list(_lex())`.

        "You wouldn't list() a socket!"
        """

        # Paranoia check:
        # There should only ever be precisely one iterator stream!
        assert self._iterobj is None
        self._compile()
        yield None
        assert self._iterobj is not None

        while True:
            # Optimized assuming that the next byte is *usually present*
            try:
                data = self._buffer[0]
            except IndexError:
                try:
                    data = yield from self.peek()
                except EOFError:
                    return

            if data in STRUCTURAL_LUT:
                # Is this a structural token?
                kind = STRUCTURAL_LUT[data]
                self._advance()
                yield Token(kind, self._pos - 1, self._line, self._pos, self._line)
            else:
                # Wasn't a structural token, use the lexing LUT.
                try:
                    yield from self._table[data]()
                except EOFError:
                    # One of the delegate lexers encountered EOF when
                    # more data is required to form a valid token.
                    yield from self._reject_token(
                        f"Unexpected EOF in {self._pend.name.lower()}", eof=True
                    )
