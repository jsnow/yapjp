#!/usr/bin/env python3
import json

from yapjp import Parser, parse
from yapjp import Lexer, LexerError
from yapjp.lexer import TokenKind, Position


def print_error_msg(data, emit_junk: bool):
    lex = Lexer()
    lex.opt.emit_junk = emit_junk

    lex.feed(data)
    lex.feed_eof()

    try:
        token = next(lex)
    except LexerError as exc:
        if "\n" in str(exc):
            print(f"LexerError:\n{str(exc)}")
        else:
            print(f"LexerError: {str(exc)}")
        #if exc.__cause__ and not isinstance(exc.__cause__, EOFError):
        #    print(f"... {type(exc.__cause__).__name__}: {str(exc.__cause__)}")
    else:
        print(token)
        token.print_detail()


def tostr(data: bytes) -> str:
    return data.decode('UTF-8', errors='backslashreplace')


def _negative_test(send_data, emit_junk, ref, subref = None):
    lex = Lexer()
    lex.opt.emit_junk = emit_junk
    lex.feed(send_data)
    lex.feed_eof()

    submsg = None
    token = None
    try:
        token = next(lex)
    except LexerError as exc:
        assert emit_junk == False
        emsg = str(exc)
        if exc.__cause__ or exc.__context__:
            subexc = exc.__cause__ or exc.__context__
            submsg = str(subexc) or None
            disambig = f"(LexerError => {type(subexc).__name__})"
        else:
            disambig = "(LexerError)"
    else:
        assert emit_junk == True
        emsg = str(token)
        disambig = "(T_JUNK)"

    if emsg == ref and submsg == subref:
        print(f"{tostr(send_data)!r:60} OK {disambig}")
    else:
        print(f"{tostr(send_data)!r:60} FAIL")
        print(f"\tref: {ref}")
        print(f"\tgot: {emsg}")
        if subref or submsg:
            print(f"\tref: {subref}")
            print(f"\tgot: {submsg}")


def test_bad_escape(emit_junk):
    # Unrecognized escape, with trailing bits
    data = b'"Oh my god! I have an \\z in my head!"'
    if emit_junk:
        ref = f"<T_JUNK {tostr(data)!r} @ [0x00 - 0x24] (Invalid string; Unrecognized escape)>"
    else:
        ref = f"Invalid string, partial token was {tostr(data[:0x18])!r} @ [0x00 - 0x17]"
        ref += "\n\tUnrecognized escape @ [0x16 - 0x17]"
    _negative_test(data, emit_junk, ref)


def test_malformed_unicode_escape(emit_junk):
    # Malformed unicode escape, with trailing bits
    data = b'"You are likely to be eaten by a \\uGRUE!"'
    if emit_junk:
        ref = f"<T_JUNK {tostr(data)!r} @ [0x00 - 0x28] (Invalid string; Invalid unicode escape)>"
    else:
        ref = f"Invalid string, partial token was {tostr(data[:0x24])!r} @ [0x00 - 0x23]"
        ref += "\n\tInvalid unicode escape @ [0x21 - 0x23]"
    _negative_test(data, emit_junk, ref)


def test_malformed_number(data, emsg, emit_junk):
    loc = f"[0x00 - 0x{len(data) - 1:02x}]"
    if emit_junk:
        msg = f"Invalid number; {emsg}"
        ref = f"<T_JUNK {tostr(data)!r} @ {loc} ({msg})>"
    else:
        ref = f"Invalid number, got {tostr(data)!r} @ {loc}"
        ref += f"\n\t{emsg} @ [0x{len(data) - 1:02x}]"
    _negative_test(data, emit_junk, ref)


def test_rejected_literal(expected, malformed, emit_junk):

    # Find rejection index; the index of first difference.
    for i, (l, r) in enumerate(zip(expected, malformed)):
        if l != r:
            break

    emsg = f"Expected literal {tostr(expected)!r}"

    if emit_junk:
        location = Position(0, len(malformed))
        ref = f"<T_JUNK {tostr(malformed)!r} @ {location} ({emsg})>"
    else:
        location = Position(0, i + 1)
        ref = f"{emsg}, but got {tostr(malformed[:i+1])!r} @ {location}"

    _negative_test(malformed, emit_junk, ref)


def test_junk(junk, emit_junk):
    location = Position(0, len(junk))
    if emit_junk:
        ref = f"<T_JUNK {tostr(junk)!r} @ {location}>"
    else:
        ref = f"Unexpected byte {chr(junk[0])!r} @ [0x00]"
    _negative_test(junk, emit_junk, ref)


def test_ascii_control_chars(emit_junk):
    for invalid_byte in range(0x20):
        data = b'"I can eat ' + bytes((invalid_byte,)) + b', it does not harm me."'
        if emit_junk:
            emsg = "Invalid string; Illegal character"
            loc = Position(0, len(data))
            ref = f"<T_JUNK {tostr(data)!r} @ {loc} ({emsg})>"
        else:
            emsg = f"Invalid string"
            partial = f"{tostr(data[:0x0c])!r}"
            loc = Position(0, 0x0c)
            ref = f"{emsg}, partial token was {partial} @ {loc}"
            ref += f"\n\tIllegal character @ [0x0b]"
        _negative_test(data, emit_junk, ref)


def test_bad_unicode(start, end, uref, emit_junk):
    for invalid_byte in range(start, end):
        seq = b'"I can eat ' + bytes((invalid_byte,)) + b', it does not harm me."'
        loc = "[0x00 - 0x22]"
        emsg = "Failed to finalize T_STRING data"
        subref = (
            f"'utf-8' codec can't decode byte 0x{invalid_byte:02x} "
            f"in position 11: {uref}"
        )

        if emit_junk:
            ref = f"<T_JUNK {tostr(seq)!r} @ {loc} ({emsg}; {subref})>"
            subref = None
        else:
            ref = f"{emsg}, data was {tostr(seq)!r} @ [0x00 - 0x22]"
            ref += f"\n\t{subref} @ [0x0b]"
        _negative_test(seq, emit_junk, ref, subref)


def test_eof(data, what, emit_junk):
    if len(data) == 1:
        loc = "[0x00]"
    else:
        loc = f"[0x00 - 0x{len(data) - 1:02x}]"

    if emit_junk:
        ref = f"<T_JUNK {tostr(data)!r} @ {loc} (Unexpected EOF in {what})>"
    else:
        ref = f"Unexpected EOF in {what}, partial token was {tostr(data)!r} @ {loc}"

    _negative_test(data, emit_junk, ref)


def test_ascii():
    for char_byte in range(0x00, 0x80):
        lex = Lexer()

        if char_byte <= 0x1F:
            # These need to be very especially escaped :)
            seq = b'"\\u' + f"{char_byte:04x}".encode() + b'"'
        elif char_byte in (0x22, 0x5c):
            # These need to be escaped
            seq = b'"' + bytes((0x5c, char_byte)) + b'"'
        else:
            seq = b'"' + bytes((char_byte,)) + b'"'
        lex.feed(seq)
        token = next(lex)

        reference = json.loads(seq)
        if token.value == reference:
            print(f"{seq!r:60} OK {token.value!r}")
        else:
            print(f"{seq!r:60} FAIL {token.value!r}")


def test(data, ref_value):
    a = list(parse(data))
    assert len(a) == 1
    a = a[0]

    b = json.loads(data)

    if a == b and a == ref_value:
        print(f"{data!r:60} OK {ref_value!r}")
    else:
        print(f"{data!r:60} FAIL {a!r} {ref_value!r}")


if __name__ == '__main__':
    test(b'true', True)
    test(b'false', False)
    test(b'null', None)

    teststr = "I can eat glass, it does not hurt me."
    test(b'"' + teststr.encode("UTF-8") + b'"', teststr)

    teststr = "hello \\r\\n\\b\\f\\t world"
    test(b'"' + teststr.encode("UTF-8") + b'"', "hello \r\n\b\f\t world")

    teststr = r"hello \\ \/ world"
    test(b'"' + teststr.encode("UTF-8") + b'"', r"hello \ / world")

    teststr = r"hello \" world"
    test(b'"' + teststr.encode("UTF-8") + b'"', 'hello " world')

    test(b'"' + "unicode: \U0001F40D".encode("UTF-8") + b'"', "unicode: \U0001F40D")
    test(b'"' + "surrogates: \\uD83D\\uDC0D".encode("UTF-8") + b'"', "surrogates: \U0001F40D")
    test(b'"' + "orphaned surrogate: \\uD83D".encode("UTF-8") + b'"', "orphaned surrogate: \uD83D")

    test(b'0', 0)
    test(b'-0', -0)
    test(b'1', 1)
    test(b'10', 10)
    test(b'2147483647', 2**31-1)  # int32_t
    test(b'-2147483648', -2**31)  # int32_t
    test(b'4294967295', 2**32-1)  # uint32_t
    test(b'9223372036854775807', 2**63-1)  # int64_t
    test(b'-9223372036854775808', -2**63)  # int64_t
    test(b'18446744073709551615', 2**64-1)  # uint64_t

    test(b'0.0', 0.0)
    test(b'0.1', 0.1)
    test(b'0.01', 0.01)
    test(b'-0.0', -0.0)
    test(b'-0.1', -0.1)
    test(b'-0.01', -0.01)

    test(b'[]', [])
    test(b' [ ] ', [])
    test(b'{}', {})
    test(b' { } ', {})

    test(b' [ null ] ', [None])
    test(b' [ true , false ] ', [True, False])

    test(b'[null]', [None])
    test(b'[true,false]', [True, False])
    test(b' [ null]', [None])
    test(b'[null ] ', [None])
    test(b' [ true]', [True])
    test(b'[true ] ', [True])
    test(b' [ false]', [False])
    test(b'[false ] ', [False])

    test(b'["Nested", ["Arrays"]]', ["Nested", ["Arrays"]])

    # Test bytes 0x00 through 0x7f, escaping when needed.
    test_ascii()

    eof_sequences = [
        (b'nul', 'literal'),       # Unexpected EOF in literal
        (b'Infinit', 'number'),    # Unexpected EOF in numerical literal
        (b'-Inf', 'number'),       # Unexpected EOF in numerical literal
        (b'Na', 'literal'),        # Unexpected EOF in numerical literal
        (b'-', 'number'),          # Unexpected EOF in number (integral component)
        (b'0.', 'number'),         # Unexpected EOF in number (fractional component)
        (b'1337.', 'number'),      # Unexpected EOF in number (fractional component)
        (b'42.0e', 'number'),      # Unexpected EOF in number (exponential component)
        (b'42.0E', 'number'),      # Unexpected EOF in number (exponential component)
        (b'42.0e-', 'number'),     # Unexpected EOF in number (exponential component)
        (b'42.0E-', 'number'),     # Unexpected EOF in number (exponential component)
        (b'42.0e+', 'number'),     # Unexpected EOF in number (exponential component)
        (b'42.0E+', 'number'),     # Unexpected EOF in number (exponential component)
        (b'"Candleja', 'string'),  # Unexpected EOF in string
    ]

    # Error message tests:
    for emit_junk in (True, False):
        if emit_junk:
            print("\n== Error Message Tests (T_JUNK) ==\n")
        else:
            print("\n== Error Message Tests (LexerError) ==\n")

        test_rejected_literal(b'true', b'truf', emit_junk)
        test_rejected_literal(b'false', b'falso', emit_junk)
        test_rejected_literal(b'null', b'nulp', emit_junk)

        test_rejected_literal(b'NaN', b'Nah', emit_junk)
        test_rejected_literal(b'Infinity', b'Infinitee', emit_junk)
        test_rejected_literal(b'-Infinity', b'-Infiniband', emit_junk)

        test_junk(b'junk', emit_junk)
        for (data, what) in eof_sequences:
            test_eof(data, what, emit_junk)

        # Test unrecognized escapes
        test_bad_escape(emit_junk)
        test_malformed_unicode_escape(emit_junk)

        # Test bytes 0x00 through 0x1f, without escaping.
        test_ascii_control_chars(emit_junk)

        # Test bytes 0x80 through 0xFF, without escaping.
        # Only ever used as continuation bytes:
        test_bad_unicode(0x80, 0xC0, "invalid start byte", emit_junk)

        # Invalid start bytes: only start 2 byte overlong sequences
        # Never a valid byte.
        test_bad_unicode(0xC0, 0xC2, "invalid start byte", emit_junk)

        # Valid start bytes, but not by their lonesome.
        # 0xC2 - 0xDF: 2 byte sequences
        # 0xE0 - 0xEF: 3 byte sequences
        # 0xF0 - 0xF4: 4 byte sequences
        test_bad_unicode(0xC2, 0xF5, "invalid continuation byte", emit_junk)

        # Invalid start bytes:
        # 0xF5 - 0xF7: 4 byte sequences (beyond U+10FFFF)
        # 0xF8 - 0xFB: 5 byte sequences (beyond U+10FFFF)
        # 0xFC - 0xFD: 6 byte sequences (beyond U+10FFFF)
        # 0xFE - 0xFF: Undefined by unicode.
        test_bad_unicode(0xF5, 0x100, "invalid start byte", emit_junk)

        # Malformed numbers
        bad = [
            (b'-e', 'Expected integral digit [1-9]'),
            (b'5.E', 'Expected fractional digit [0-9]'),
            (b'42ez', 'Expected exponential digit [0-9]'),
            (b'42E+z', 'Expected exponential digit [0-9]'),
        ]
        for data, emsg in bad:
            test_malformed_number(data, emsg, emit_junk)


    print("\n\n=== Error Message Reference ===\n")

    # This list should have 100% coverage over every LexerError emitted.

    data_sequences = [
        # Literals
        b'truf',       # Rejected literal

        # Junk
        b'junk',       # Unexpected byte(s)/junk

        # EOF
        b'nul',        # Unexpected EOF in literal
        b'-Inf',       # Unexpected EOF in numerical literal
        b'Na',         # Unexpected EOF in numerical literal
        b'-',          # Unexpected EOF in number (integral component)
        b'0.',         # Unexpected EOF in number (fractional component)
        b'1337.',      # Unexpected EOF in number (fractional component)
        b'42.0e',      # Unexpected EOF in number (exponential component)
        b'"Candleja',  # Unexpected EOF in string

        # Strings
        b'"\x80"',                # Illegal byte (UnicodeDecodeError)
        b'"\x00"',                # Illegal control character
        b'"\\z"',                 # Unrecognized escape
        b'"Hello \\z world"',     # Unrecognized escape, with trailing bits
        b'"\\uFFFG"',             # Invalid unicode escape
        b'"\\uFFFGood Golly!!"',  # Invalid unicode, with trailing bits

        # Numbers
        b'-e',         # Expecting digit (integral component)
        b'5.E',        # Expecting digit (fractional component)
        b'42ez',       # Expecting digit (exponent component)
        b'42E+z',      # Expecting digit (exponent component, with sign)

        # Speshal Numberz - FIXME needs unit tests
        b'Nah',
        b'Infinitee',
        b'-Infiniband',
    ]

    for data in data_sequences:
        print(f"# {data}")
        print_error_msg(data, False)
        print_error_msg(data, True)
        print("")
